# Sum ML

I want to introduce you to Sum ML. Sum ML is an app using tensorflow models to provide basic summarisations and text classifications directly on you smartphone without needing a network connection or sending data through the internet. It is a privacy friendly way to quickly get the gist in the fast news circle we are living in. 

This is the git for my thesis at the Technische Hochschule Köln. The goal was to develop an android app that helps with summarisation tasks. In the end only a classification task works reliably, the summarisation models still having issues with the summarisation itself. In this repository I provide all code necessary to build the application and train the models itself. 

The models used in this project were provided by other people, the RNN Model was written by Chen and published under the MIT License [here](https://github.com/chen0040/keras-text-summarization)
Another model by Logothetis was also published under the MIT License [here](https://github.com/flogothetis/Abstractive-Summarization-T5-Keras).

## Links

* [Changelog](/CHANGELOG.md)
* [License](/LICENSE)
* [Model Notebooks](/tensorflow/code)
* [Android Studio Project](/SumML)

## Versions

* 1.0 – Finished thesis
  * working classification
  * summarisation models are implemented, not working
  * RNN model is active for summarisation fragment
  * Code TODOs left

## Future

* After the thesis it is planned, to further improve the project and maybe publish it later

## App Design

A mockup created for the thesis. Not all fragments work as presented:

![The mockup of the app](imgs/summl-ui.png)