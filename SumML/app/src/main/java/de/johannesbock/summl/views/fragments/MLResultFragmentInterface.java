package de.johannesbock.summl.views.fragments;

import java.util.List;

import de.johannesbock.summl.mltasks.MLResult;

/**
 * Each Fragment that shows the Results of a Machine Learning task in this app should at least
 * have these functions.
 */
public interface MLResultFragmentInterface {

    /**
     * This should be implemented to show the results of the task in the fragment.
     * @param inputData Should hold the input text, so this can be displayed if wished
     * @param results These are the results, of the ML task in a list.
     */
    void showResults(final String inputData,
                     final List<MLResult> results);

    /**
     * This is the function to like the result of the ML task
     */
    void onLike();

    /**
     * This is the function to dislike the result ot the ML task
     */
    void onDislike();

    /**
     * Trigger a button to start processing the input data.
     * @param inputData The input data, that should be processed.
     */
    void processInput(final String inputData);

}
