package de.johannesbock.summl.views;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;
import com.google.android.material.navigation.NavigationView;

import de.johannesbock.summl.R;
import de.johannesbock.summl.views.fragments.ClassificationFragment;
import de.johannesbock.summl.views.fragments.HomeFragment;
import de.johannesbock.summl.views.fragments.SummaryFragment;

public class MainActivity extends AppCompatActivity {

    private final static String TAG = MainActivity.class.getName();

    BottomNavigationView bottomNavigationView;
    NavHostFragment navHostFragment;
    NavController navController;

    // The fragments
    HomeFragment homeFragment = new HomeFragment();
    SummaryFragment summaryFragment = new SummaryFragment();
    ClassificationFragment classificationFragment = new ClassificationFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        bottomNavigationView = findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setOnItemSelectedListener(navigationItemSelectedListener);
        bottomNavigationView.setSelectedItemId(R.id.homeFragment);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragmentContainer, homeFragment)
                .commit();

    }


    private final NavigationBarView.OnItemSelectedListener navigationItemSelectedListener =
            new NavigationBarView.OnItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                    // TODO: enable the other fragments
                    if(item.getItemId() == R.id.homeFragment) {
                        getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.fragmentContainer, homeFragment)
                                .commit();
                    } else if (item.getItemId() == R.id.summaryFragment) {
                        getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.fragmentContainer, summaryFragment)
                                .commit();
                    } else if (item.getItemId() == R.id.classificationFragment) {
                        getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.fragmentContainer, classificationFragment)
                                .commit();
                    }

                    return true;
                }
            };

}