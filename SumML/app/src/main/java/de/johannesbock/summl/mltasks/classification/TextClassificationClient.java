package de.johannesbock.summl.mltasks.classification;

import android.content.Context;
import android.util.Log;

import org.tensorflow.lite.task.text.nlclassifier.NLClassifier;
import org.tensorflow.lite.support.label.Category;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.johannesbock.summl.mltasks.MLResult;

/**
 * This client loads the model and provides the prediction API.
 */

public class TextClassificationClient {

    private static final String TAG = TextClassificationClient.class.getName();
    private static final String MODEL_PATH = "text_classification_v2.tflite";

    private final Context context;

    NLClassifier classifier;


    public TextClassificationClient(Context context) {
        this.context = context;
    }

    /**
     * Loads the Natural Language Text Classifier and the model to create the classifier.
     */
    public void load() {
        try {
            classifier = NLClassifier.createFromFile(context, MODEL_PATH);
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    /**
     * Closes the Classifier to reset the classifier
     */
    public void unload() {
        classifier.close();
        classifier = null;
    }

    /**
     * This function allows to classify the text that was put into the app by the user.
     * @param text The input data that should be classified
     * @return A list with the results of the classification.
     */
    public List<MLResult> classify(String text) {
        List<Category> apiResults = classifier.classify(text);
        List<MLResult> textClassifierResults = new ArrayList<>(apiResults.size());

        for(int i = 0; i < apiResults.size(); i++) {
            Category category = apiResults.get(i);
            textClassifierResults.add(new TextClassificationResult(
                            "" + i,
                            category.getLabel(),
                            category.getScore()));
        }

        Collections.sort(textClassifierResults);

        return textClassifierResults;

    }


}
