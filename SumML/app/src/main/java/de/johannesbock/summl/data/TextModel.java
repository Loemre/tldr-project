package de.johannesbock.summl.data;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * @author johannesbock
 * This entity describes the model to represent the text in the database.
 * Can store the input data and the summary with information from tensorflow and will be
 * extended further.
 *
 * The table holds the following parameters:
 * <table>
 *     <th>
 *         <td>Type</td>
 *         <td>Name</td>
 *         <td>Data represented</td>
 *     </th>
 *     <tr>
 *         <td>int</td>
 *         <td>textId</td>
 *         <td>The primary key, to identify the input data</td>
 *     </tr>
 *     <tr>
 *         <td>String</td>
 *         <td>inputData</td>
 *         <td>The text or other data, that should be processed by the application</td>
 *     </tr>
 *     <tr>
 *         <td>String</td>
 *         <td>summary</td>
 *         <td>The summary processed by the machine learning algortihms.</td>
 *     </tr>
 *     <tr>
 *         <td>String</td>
 *         <td>date</td>
 *         <td>Date of the text</td>
 *     </tr>
 *     <tr>
 *         <td>boolean</td>
 *         <td>testData</td>
 *         <td>This is for the test data, that should not be available in production</td>
 *     </tr>
 *     <tr>
 *         <td>later</td>
 *         <td>tensorflowParameters</td>
 *         <td>To store needed tensorflow parameters</td>
 *     </tr>
 * </table>
 *
 */

@Entity(tableName = "input_data_table")
public class TextModel {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int textId;

    @NonNull
    private String inputData;

    @Nullable
    private String title;

    @Nullable
    private String summary;

    @Nullable
    private String date;

    @Nullable
    private int secondsToSummarise;

    @Nullable
    private boolean testData;

    @Nullable
    private float confidence;

    @Nullable
    private boolean quality;



    // Constructor

    public TextModel(@NonNull String inputData) {
        this.inputData = inputData;
    }

    // Getter and Setter

    public int getTextId() {
        return textId;
    }

    public void setTextId(int textId) {
        this.textId = textId;
    }

    @NonNull
    public String getInputData() {
        return inputData;
    }

    public void setInputData(@NonNull String inputData) {
        this.inputData = inputData;
    }

    @Nullable
    public String getTitle() {
        return title;
    }

    public void setTitle(@Nullable String title) {
        this.title = title;
    }

    @Nullable
    public String getSummary() {
        return summary;
    }

    public void setSummary(@Nullable String summary) {
        this.summary = summary;
    }

    @Nullable
    public String getDate() {
        return date;
    }

    public void setDate(@Nullable String date) {
        this.date = date;
    }

    @Nullable
    public boolean isTestData() {
        return testData;
    }

    public void setTestData(boolean testData) {
        this.testData = testData;
    }

    @Nullable
    public float getConfidence() {
        return confidence;
    }

    public void setConfidence(float confidence) {
        this.confidence = confidence;
    }

    @Nullable
    public boolean isQuality() {
        return quality;
    }

    public void setQuality(boolean quality) {
        this.quality = quality;
    }

    @Nullable
    public int getSecondsToSummarise() {
        return secondsToSummarise;
    }

    public void setSecondsToSummarise(int secondsToSummarise) {
        this.secondsToSummarise = secondsToSummarise;
    }
}
