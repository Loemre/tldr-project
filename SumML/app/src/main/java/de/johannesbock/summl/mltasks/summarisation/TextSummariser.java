package de.johannesbock.summl.mltasks.summarisation;

import java.io.IOException;
import java.util.List;

import de.johannesbock.summl.mltasks.MLResult;

/**
 * The interface that will serve as text summariser to implement different models.
 * With this interface the app should be able to interact with different summarisation engines.
 */
public interface TextSummariser {

    /**
     * The implementation of this function should implement the summarisation tasks.
     * @Param   texts   A string holding the text to summarise
     * @return          The summarisation as a string.
     */
    String summariseText(String inputText);

    /**
     * This function should implement a logging feature for the machine learning tasks.
     * @param   debug   Is this a debugging run or not
     */
    void enableLogging(final boolean debug);

    /**
     * This function opens the summariser and initialises it.
     */
    void load() throws IOException;

    /**
     * This function closes the summariser and deletes all unnecessary elements.
     */
    void close();
}
