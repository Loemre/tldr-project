package de.johannesbock.summl.models;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import de.johannesbock.summl.data.TextModel;
import de.johannesbock.summl.data.TextModelDatabase;

/**
 * This class handles the data from the database to make it accessible for the views.
 */

public class SharedDataViewModel extends AndroidViewModel {

    public final static String TAG = SharedDataViewModel.class.getName();

    private TextModelDatabase textModelDatabase;
    private LiveData<List<TextModel>> textModelListLiveData;

    public SharedDataViewModel(@NonNull Application application) {
        super(application);

        textModelDatabase = TextModelDatabase.getInstance(application.getApplicationContext());
        textModelListLiveData = textModelDatabase.textModelDAO().getAllTexts();

    }

    public LiveData<List<TextModel>> getTextModelListLiveData() {
        return textModelListLiveData;
    }

}
