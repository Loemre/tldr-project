package de.johannesbock.summl.data;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

/**
 * With the DAO the database can be accessed and the functions to operate on the database are
 * provided to other classes.
 */

@Dao
public interface TextModelDAO {

    // If there is already a text, it will be ignored
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(TextModel textModel);

    // Update a specific text
    @Update
    void update(TextModel textModel);

    // delete everything from the table
    @Query("DELETE FROM input_data_table")
    void deleteAll();

    // delete specific text from the table
    @Query("DELETE FROM input_data_table WHERE textId = :textId")
    void deleteInputData(int textId);

    // Returns a list with all texts in the database, that updates on change
    @Query("SELECT * FROM input_data_table")
    LiveData<List<TextModel>> getAllTexts();

    // TODO: MAKE SEARCH AVAILABLE

    // Add more queries on the database here later, just the basic implementation above

}
