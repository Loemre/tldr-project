package de.johannesbock.summl.mltasks;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import org.tensorflow.TensorFlow;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class provides functions and constants that can and should be
 * used over all ML classes.
 */
public class MLUtils {

    private static final String TAG = MLUtils.class.getName();

    public static final int MAX_INPUT_SEQUENCE_LENGTH = 500;
    public static final int MAX_TARGET_SEQUENCE_LENGTH = 50;

    private static final Gson gson = new Gson();

    public static List<Float> padSequences(List<Float> sequence, int maxLen, boolean pre, Float padValue) {
        List<Float> paddedSequence = new ArrayList<>();

        // TODO: if larger than maxlen shorten, new parameter if front or back
        if(sequence.size() >= maxLen) {
            paddedSequence = sequence;
        } else {
            int valuesToFill = maxLen - sequence.size();
            if(pre) {
                for(int i = 0; i < valuesToFill; i++) {
                    paddedSequence.add(padValue);
                }
                paddedSequence.addAll(sequence);
            } else {
                paddedSequence.addAll(sequence);
                for(int i = 0; i < valuesToFill; i++) {
                    paddedSequence.add(padValue);
                }
            }
        }



        return paddedSequence;
    }

    public static Map<String, Float> getWord2Index(Context context, String fileName) {
        String jsonString = readJsonFromFile(context, fileName);

        Map<String,Float> word2index = new HashMap<String,Float>();

        Map map = gson.fromJson(jsonString, Map.class);

        map.forEach((key, value) -> {
            Double doubleValue = (Double) value;
            word2index.put((String) key, doubleValue.floatValue());
        });

        return word2index;
    }

    public static Map<Integer, String> getIndex2Word(Context context, String fileName) {
        String jsonString = readJsonFromFile(context, fileName);

        Map<Integer,String> index2word = new HashMap<Integer,String>();

        Map map = gson.fromJson(jsonString, Map.class);

        map.forEach((key, value) -> {
            String keyValue = (String) key;
            index2word.put(Integer.parseInt(keyValue), (String) value);
        });

        return index2word;
    }

    private static String readJsonFromFile(Context context, String fileName) {
        String json = null;

        try {
            InputStream inputStream = context.getAssets().open(fileName);
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            json = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
        }

        return json;
    }

}
