package de.johannesbock.summl.services;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;

import android.os.Handler;
import android.util.Log;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import de.johannesbock.summl.data.TextModel;
import de.johannesbock.summl.data.TextModelDatabase;
import de.johannesbock.summl.mltasks.MLResult;
import de.johannesbock.summl.mltasks.classification.TextClassificationClient;
import de.johannesbock.summl.mltasks.summarisation.TextSummarisationClient;

/**
 * Class to handle the text processing in background to not block the UI thread.
 * params should have the Extras "inputData" with the inputData and the information,
 * which text service should be used. Therefore the constants in this class are public to use.
 */
public class TextJobService extends JobService {

    private static final String TAG = TextJobService.class.getName();

    // Job constants for handling
    public final static String TEXT_SERVICE_SUMMARISATION = "summary";
    public final static String TEXT_SERVICE_CLASSIFICATION = "classification";


    // Job Service handling
    private boolean jobCancelled = false;
    private boolean available = false;

    private Context context;

    // Text processing services
    private TextClassificationClient textClassificationClient;
    private TextSummarisationClient textSummarisationClient;

    private String textService;

    final Handler handler = new Handler();



    @Override
    public boolean onStartJob(JobParameters params) {

        Log.d(TAG, "Job started");

        context = this.getApplicationContext();

        textClassificationClient = new TextClassificationClient(context);
        textSummarisationClient = new TextSummarisationClient(context);

        textService = params.getExtras().getString("textService");
        String inputData = params.getExtras().getString("inputData");

        if(textService.equals(TEXT_SERVICE_SUMMARISATION)) {

            handler.post(
                    () -> {

                        Log.d(TAG, "load summary model");
                        try {
                            textSummarisationClient.load();
                            summariseText(inputData);
                        } catch (IOException e) {
                            e.printStackTrace();
                            // TODO: handle correctly
                            onStopJob(params);

                        }

                    });

        } else if (textService.equals(TEXT_SERVICE_CLASSIFICATION)) {

            handler.post(
                    textClassificationClient::load);

            classifyText(inputData);
        }

        return true;
    }



    @Override
    public boolean onStopJob(JobParameters params) {

        Log.d(TAG, "Job cancelled before completion");

        jobCancelled = true;

        if(textService.equals(TEXT_SERVICE_CLASSIFICATION)) {

            handler.post(
                    textClassificationClient::unload);


        } else if (textService.equals(TEXT_SERVICE_SUMMARISATION)) {

            handler.post(
                    () -> {
                        Log.d(TAG, "summarisation client closed");
                        textSummarisationClient.close();
                    });
        }

        return true;
    }

    /**
     *
     * @param inputData
     */
    private void summariseText(String inputData) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(
                        () -> {

                            Log.d(TAG, "summary started");

                            String summary =
                                    textSummarisationClient.summariseText(inputData);

                            saveSummaryToDatabase(inputData, summary);

                        });
            }
        }).start();

    }

    /**
     *
     * @param inputData
     */
    private void classifyText(String inputData) {

        new Thread(new Runnable() {
            @Override
            public void run() {

                handler.post(
                        () -> {
                            List<MLResult> textClassifierResults =
                                    textClassificationClient.classify(inputData);

                            textClassificationClient.unload();

                            saveToDatabase(textClassifierResults, inputData);
                        });
            }
        }).start();

    }

    private void saveToDatabase(List<MLResult> results, String inputData) {

        for(MLResult result : results) {
            TextModel textModel = new TextModel(inputData);
            textModel.setSummary(result.getTitle());
            textModel.setDate(new Date().toString());
            textModel.setConfidence(result.getConfidence());


            TextModelDatabase.getInstance(context).textModelDAO().insert(textModel);
        }

    }

    private void saveSummaryToDatabase(String inputData, String summary) {
        TextModel textModel = new TextModel(inputData);
        textModel.setSummary(summary);
        textModel.setDate(new Date().toString());

        TextModelDatabase.getInstance(context).textModelDAO().insert(textModel);
    }

}
