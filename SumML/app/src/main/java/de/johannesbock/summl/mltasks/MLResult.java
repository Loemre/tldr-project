package de.johannesbock.summl.mltasks;

import java.util.Locale;

/**
 * Each result of Machine Learning tasks in this application should at least look like this.
 * For more precise implementations it can extend this class and override the methods.
 */
public class MLResult implements Comparable<MLResult> {

    /**
     * Create a unique identifier for the class, not the instance, of the object.
     */
    private final String id;

    /**
     * Display name for the result.
     */
    private final String title;

    /**
     * A score that shows how good the result is compared to other results.
     */
    private final Float confidence;

    public MLResult(String id, String title, Float confidence) {
        this.id = id;
        this.title = title;
        this.confidence = confidence;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Float getConfidence() {
        return confidence;
    }

    @Override
    public String toString() {
        String result = "";

        if(id != null) {
            result += "[" + id + "] ";
        }

        if(title != null) {
            result += title + " ";
        }

        if(confidence != null) {
            result += String.format(
                    Locale.ENGLISH,
                    "(%.1f%% ", confidence * 100.0f );
        }

        return result.trim();
    }

    @Override
    public int compareTo(MLResult o) {
        return o.confidence.compareTo(confidence);
    }
}
