package de.johannesbock.summl.views.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Locale;

import de.johannesbock.summl.R;
import de.johannesbock.summl.mltasks.MLResult;
import de.johannesbock.summl.mltasks.classification.TextClassificationClient;
import de.johannesbock.summl.models.SharedDataViewModel;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ClassificationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ClassificationFragment extends Fragment implements MLResultFragmentInterface {

    private final static String TAG = ClassificationFragment.class.getName();

    // defines how the arguments should be named for the initialisation
    private static final String ARG_TEXT_ID = "textId";

    // stores the id of the text in the database
    private int mTextId;

    // implement the shared Data View Model
    private SharedDataViewModel sharedDataViewModel;

    // the elements in the view
    private TextView titleTextView;
    private TextView timeToClassifyTextView;
    private TextView classificationTextView;
    private TextView secondsClassificationTextView;
    private TextView positiveConfidencePercent;
    private TextView negativeConfidencePercent;

    private ProgressBar confidenceBar;

    private ScrollView resultScrollView;

    private EditText inputTextEdit;

    // TODO: tensorflow demo, should not stay here
    private TextClassificationClient client;

    private Handler handler;

    public ClassificationFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param textId The id of the text, that should be shown from the database.
     * @return A new instance of fragment ClassificationFragment.
     */
    public static ClassificationFragment newInstance(int textId) {
        ClassificationFragment fragment = new ClassificationFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_TEXT_ID, textId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mTextId = getArguments().getInt(ARG_TEXT_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView =
                inflater.inflate(R.layout.fragment_classification, container, false);

        client = new TextClassificationClient(rootView.getContext());
        handler = new Handler();

        // Button handling
        Button predictButton = rootView.findViewById(R.id.predictButton);
        predictButton.setOnClickListener(
                (View v) -> {
                    processInput(inputTextEdit.getText().toString());
                }
        );

        ImageButton imageButtonIsGood = rootView.findViewById(R.id.buttonIsGood);
        imageButtonIsGood.setOnClickListener(
                (View v) -> {
                    onLike();
                }
        );

        ImageButton imageButtonIsBad = rootView.findViewById(R.id.buttonIsBad);
        imageButtonIsBad.setOnClickListener(
                (View v) -> {
                    onDislike();
                }
        );

        // Get all the elements in the view
        titleTextView = rootView.findViewById(R.id.titleTextView);
        timeToClassifyTextView = rootView.findViewById(R.id.timeToClassify);
        classificationTextView = rootView.findViewById(R.id.classifyTextView);
        secondsClassificationTextView = rootView.findViewById(R.id.secondsToClassification);
        positiveConfidencePercent = rootView.findViewById(R.id.positiveConfidencePercent);
        negativeConfidencePercent = rootView.findViewById(R.id.negativeConfidencePercent);

        confidenceBar = rootView.findViewById(R.id.confidenceBar);
        confidenceBar.setProgress(50);



        inputTextEdit = rootView.findViewById(R.id.inputText);
        resultScrollView = rootView.findViewById(R.id.resultsScrollView);

        showWithNoResults();

        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.v(TAG, "onStart");
        handler.post(
                () -> {
                    client.load();
                });
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.v(TAG, "onStop");
        handler.post(
                () -> {
                    client.unload();
                });
    }


    // TODO: connect database and use arguments
    @Override
    public void showResults(final String inputData,
                            List<MLResult> results) {

        // some UI things
        String secondsToSummarise = null;
        String titleString = "Movie Review";

        // Run on the UI Thread as we are updating our UI

        getActivity().runOnUiThread(
                () -> {
                    String textToShow = "Input: " + inputData;
                    for (int i = 0; i < results.size(); i++) {
                        MLResult textClassifierResult =
                                results.get(i);

                        if (textClassifierResult.getTitle().equals("Positive")) {
                            confidenceBar.setProgress(
                                    Math.round(textClassifierResult.getConfidence() * 100));
                            positiveConfidencePercent.setText(
                                    String.format(
                                            Locale.ENGLISH,
                                            "%.2f", textClassifierResult.getConfidence()));
                        } else if (textClassifierResult.getTitle().equals("Negative")) {
                            negativeConfidencePercent.setText(
                                    String.format(
                                            Locale.ENGLISH,
                                            "%.2f", textClassifierResult.getConfidence()
                                    )
                            );
                        }

                    }

                    // fill the fields

                    titleTextView.setText(titleString);

                    if (secondsToSummarise != null) {
                        timeToClassifyTextView.setText(secondsToSummarise);
                        secondsClassificationTextView.setVisibility(View.VISIBLE);
                    } else {
                        timeToClassifyTextView.setText("");
                        secondsClassificationTextView.setVisibility(View.INVISIBLE);
                    }

                    // Append the result to the UI
                    classificationTextView.setText(textToShow);

                    // Clear the input text
                    inputTextEdit.getText().clear();

                    // Scroll to the bottom to show the latest entry's summarisation result
                    resultScrollView.post(() -> resultScrollView.fullScroll(View.FOCUS_DOWN));

                });

    }

    @Override
    public void onDislike() {
        Toast dislikeToast = Toast.makeText(
                getActivity().getApplicationContext(),
                "disliked the summary",
                Toast.LENGTH_SHORT
        );
        dislikeToast.show();
        // TODO: Add the database connection here
    }

    @Override
    public void onLike() {
        Toast likeToast = Toast.makeText(
                getActivity().getApplicationContext(),
                "liked the summary",
                Toast.LENGTH_SHORT);

        likeToast.show();

        // TODO: Add the database connection here
    }

    @Override
    public void processInput(String inputData) {
        handler.post(
                () -> {
                    // Run the text summarisation with TFLite
                    List<MLResult> textClassifierResults = client.classify(inputData);

                    // Show classification result on the screen
                    showResults(inputData, textClassifierResults);
                });
    }

    /**
     * The basic state the fragment gets loaded in
     */
    public void showWithNoResults() {

        String titleString = "Enter Movie Review";
        String enterMovieReview = "To show a classification, enter a movie review below.";

        titleTextView.setText(titleString);

        timeToClassifyTextView.setText("");
        secondsClassificationTextView.setVisibility(View.INVISIBLE);

        classificationTextView.setText(enterMovieReview);

        // Clear the input text
        inputTextEdit.getText().clear();

    }

}