package de.johannesbock.summl.mltasks.summarisation;

import android.content.Context;
import android.util.Log;

import org.tensorflow.lite.DataType;
import org.tensorflow.lite.Interpreter;
import org.tensorflow.lite.support.tensorbuffer.TensorBuffer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import de.johannesbock.summl.ml.SummarizerV1;
import de.johannesbock.summl.mltasks.MLResult;
import de.johannesbock.summl.mltasks.MLUtils;

/**
 * This client loads a text summarisation tensoflow lite model and processes the data to display
 * the results. It then provides the summarisation prediction API.
 */

public class TextSummarisationClient implements TextSummariser {

    private static final String TAG = TextSummarisationClient.class.getName();
    private static final String MODEL_PATH = "summarizer_v1.tflite";

    private final Context context;

    private final Map<String, Float> input_word2index;
    private final Map<Integer, String> input_index2word;
    private final Map<String, Float> target_word2index;
    private final Map<Integer, String> target_index2word;

    Interpreter tensorflowInterpreter;
    SummarizerV1 summarizerModel;
    SummarizerV1.Outputs outputs;

    public TextSummarisationClient(Context context) {
        this.context = context;
        input_word2index = MLUtils.getWord2Index(context, "input_word2index.json");
        input_index2word = MLUtils.getIndex2Word(context, "input_index2word.json");
        target_word2index = MLUtils.getWord2Index(context,"target_word2index.json");
        target_index2word = MLUtils.getIndex2Word(context,"target_index2word.json");
    }

    @Override
    public void enableLogging(boolean debug) {

    }

    /**
     * Summarises the text
     * The input text will be preprocessed to fit the model. Than the outputs of the model will
     * be decoded and the model runs until the end token is found or the max length of the summary
     * is reached.
     * @param inputText the text that will be put into the model
     * @return the summary processed by the model
     */
    @Override
    public String summariseText(String inputText) {
        StringBuilder summary = new StringBuilder();
        boolean modelTerminated = false;

        Log.d(TAG, "summary started");

        List<Float> encodedInputText = encodeText(inputText);

        // Pad the sequence for the model
        encodedInputText = MLUtils.padSequences(encodedInputText,
                MLUtils.MAX_INPUT_SEQUENCE_LENGTH,
                true,
                input_word2index.get("PAD"));

        Float startToken = target_word2index.get("START");

        List<Float> targetList = new ArrayList<>();
        List<Float> targetListInput = new ArrayList<>();
        targetList.add(startToken);

        float[] inputArray = new float[MLUtils.MAX_INPUT_SEQUENCE_LENGTH];
        float[] targetArray = new float[MLUtils.MAX_TARGET_SEQUENCE_LENGTH];
        int i = 0;
        for (Float f:
                encodedInputText) {
            inputArray[i++] = (f != null ? f : 0f);
        }

        while (!modelTerminated) {

            targetListInput = MLUtils.padSequences(targetList,
                    MLUtils.MAX_TARGET_SEQUENCE_LENGTH,
                    true,
                    input_word2index.get("PAD"));

            TensorBuffer modelInput = TensorBuffer.createFixedSize(new int[]{1, 500}, DataType.FLOAT32);
            modelInput.loadArray(inputArray);
            TensorBuffer modelTarget = TensorBuffer.createFixedSize(new int[]{1, 50}, DataType.FLOAT32);
            i = 0;
            for(Float f :
                    targetListInput) {
                targetArray[i++] = (f != null ? f : 0f);
            }
            modelTarget.loadArray(targetArray);

            outputs = summarizerModel.process(modelTarget, modelInput);
            TensorBuffer outputFeature0 = outputs.getOutputFeature0AsTensorBuffer();
            float[] outputData = outputFeature0.getFloatArray();
            // TODO: not + 1 but for now needed, to not cancel directly
            int wordIndex = findIndexOfHighestValue(outputData) + 1;
            String outputWord = target_index2word.get(wordIndex);
            targetList.add((float) wordIndex);

            if(!outputWord.equals("START") && !outputWord.equals("END")) {
                summary.append(outputWord).append(" ");
            }

            if (outputWord.equals("END") || targetList.size() >= MLUtils.MAX_TARGET_SEQUENCE_LENGTH) {
                modelTerminated = true;
                Log.d(TAG, "output data final: " + Arrays.toString(outputData));
            }

        }

        Log.d(TAG, "summary: " + summary.toString());
        return summary.toString();
    }

    /**
     * Loads the interpreter for the Summariser class with the model provided.
     */
    @Override
    public void load() throws IOException {
            summarizerModel = SummarizerV1.newInstance(context);
    }

    /**
     * Closes the interpreter for the summariser
     */
    @Override
    public void close() {
        summarizerModel.close();
        summarizerModel = null;
    }

    /**
     * This functions encodes the text for the model. It searches the words inside the vocabulary
     * dictionary and adds the index to the list. This list will later be used to feed the model.
     * @param inputText The input text that should be summarised
     * @return a List of floats that represent the input text
     */
    private List<Float> encodeText(String inputText) {
        List<Float> encodedText = new ArrayList<>();

        inputText = inputText.toLowerCase();

        String[] words = inputText.split("\\W+");

        for (String word:
                words) {
            if(input_word2index.containsKey(word)) {
                encodedText.add(input_word2index.get(word));
            } else {
                encodedText.add(input_word2index.get("UNK"));
            }

            if (encodedText.size() >= MLUtils.MAX_INPUT_SEQUENCE_LENGTH) return encodedText;
        }

        return encodedText;
    }

    /**
     * Simple function to decode the word from the model.
     * @param index The index in the target List that was the output from the model
     * @return The word that is represented by the index
     */
    private String decodeWord(Float index) {
        String word;

        word = target_index2word.get(index);

        return word;
    }

    /**
     * Find the highest value and return the index in an array of floats
     * @param floatArray The array with floats
     * @return The index of the highest value in the array
     */
    private int findIndexOfHighestValue(float[] floatArray) {
        int index = 0;
        for(int i = 0; i < floatArray.length; i++) {
            index = floatArray[i] >floatArray[index] ? i : index;
        }

        return index;
    }

}
