package de.johannesbock.summl.mltasks.classification;

import de.johannesbock.summl.mltasks.MLResult;

/**
 * This class returns the immutable result of the summarisation.
 */
public class TextClassificationResult extends MLResult {

    /**
     * Create a unique identifier for the class, not the instance, of the object.
     */
    private final String id;

    /**
     * Display name for the result.
     */
    private final String title;

    /**
     * A score that shows how good the result is compared to other results.
     */
    private final Float confidence;

    public TextClassificationResult(String id, String title, Float confidence) {
        super(id, title, confidence);
        this.id = id;
        this.title = title;
        this.confidence = confidence;
    }



}
