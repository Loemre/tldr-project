package de.johannesbock.summl.mltasks.summarisation;

import android.content.Context;
import android.util.Log;

import org.tensorflow.lite.DataType;
import org.tensorflow.lite.support.tensorbuffer.TensorBuffer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import de.johannesbock.summl.ml.AbstractiveSummarizerV2;
import de.johannesbock.summl.mltasks.MLUtils;

public class AbstractiveSummarisationClient implements TextSummariser {

    private static final String TAG = AbstractiveSummarisationClient.class.getName();

    public final static int MAX_INPUT_LENGTH = 218;
    public final static int MAX_SUMMARY_LENGTH = 50;

    private final static String END_TOKEN = "</s>";
    private final static float UNK = 2f;

    private final Context context;

    private final Map<String, Float> vocab;

    AbstractiveSummarizerV2 summarizerModel;
    AbstractiveSummarizerV2.Outputs outputs;

    public AbstractiveSummarisationClient(Context context) {
        this.context = context;
        vocab = MLUtils.getWord2Index(context, "vocab.json");
        Log.d(TAG, "Vocab size: " + vocab.size());
    }

    @Override
    public String summariseText(String inputText) {

        // The attention masks for input text and summary
        int[] attentionMaskInput = new int[MAX_INPUT_LENGTH];
        int[] attentionMaskSummary = new int[MAX_SUMMARY_LENGTH];

        boolean modelTerminated = false;

        // fill attentions masks with the value 1
        Arrays.fill(attentionMaskInput, 1);
        Arrays.fill(attentionMaskSummary, 1);

        List<Float> encodedInputText = encodeText(inputText);
        List<Float> summaryLayerList = new ArrayList<>();
        summaryLayerList.add(UNK);

        float[] inputLayerEncoded = new float[MAX_INPUT_LENGTH];

        int i = 0;
        for (Float f:
                encodedInputText) {
            inputLayerEncoded[i++] = (f != null ? f : 0f);
        }

        Log.d(TAG, "Float Array: " + Arrays.toString(inputLayerEncoded));

        while(!modelTerminated) {
            // Creates inputs for reference.
            TensorBuffer encoderInputLayer = TensorBuffer
                    .createFixedSize(new int[]{1, 218}, DataType.FLOAT32);
            encoderInputLayer.loadArray(encodeTextOld(inputText));
            TensorBuffer summaryLayer = TensorBuffer
                    .createFixedSize(new int[]{1, 50}, DataType.FLOAT32);
            summaryLayer.loadArray(attentionMaskInput);
            TensorBuffer attentionMask = TensorBuffer
                    .createFixedSize(new int[]{1, 218}, DataType.FLOAT32);
            attentionMask.loadArray(inputLayerEncoded);
            TensorBuffer attentionMaskDecoder = TensorBuffer
                    .createFixedSize(new int[]{1, 50}, DataType.FLOAT32);
            attentionMaskDecoder.loadArray(attentionMaskSummary);

            // Runs model inference and gets result.
            outputs = summarizerModel
                    .process(encoderInputLayer, summaryLayer, attentionMask, attentionMaskDecoder);
            TensorBuffer outputFeature0 = outputs.getOutputFeature0AsTensorBuffer();

            float[] outputData = outputFeature0.getFloatArray();

            float outputWordIndex = findHighestValue(outputData);

            summaryLayerList.add(outputWordIndex);
            String outputWord = decodeText(outputWordIndex);

            if(outputWord.equals(END_TOKEN) || summaryLayerList.size() >= MAX_SUMMARY_LENGTH) {
                modelTerminated = true;
            }

            Log.d(TAG, "model finished");
        }

        return null;
    }

    @Override
    public void enableLogging(boolean debug) {

    }

    @Override
    public void load() throws IOException {
        summarizerModel = AbstractiveSummarizerV2.newInstance(context);
    }

    /**
     * Releases the model resource to prepare it for the next usage.
     */
    @Override
    public void close() {
        summarizerModel.close();
        summarizerModel = null;
    }

    private float[] encodeTextOld(String inputText) {
        float[] sequence = new float[MAX_INPUT_LENGTH];
        String[] words;

        inputText = inputText
                .replaceAll("[_\\W]", " ") // replace special characters
                .trim().replaceAll(" +", " "); // replace whitespace
        inputText = inputText.toLowerCase();

        words = inputText.split("[\\W]");

        for(int i = 0; i < words.length; i++) {
            sequence[i] = vocab.getOrDefault(words[i], UNK);

            if(i == MAX_INPUT_LENGTH-1) break;
        }

        return sequence;
    }

    private List<Float> encodeText(String inputText) {
        List<Float> encodedText = new ArrayList<>();

        inputText = inputText
                .replaceAll("[_\\W]", " ") // replace special characters
                .trim().replaceAll(" +", " "); // replace whitespace
        inputText = inputText.toLowerCase();

        String[] words = inputText.split("\\W+");

        for (String word:
                words) {
            encodedText.add(vocab.getOrDefault(word, UNK));

            if (encodedText.size() >= MAX_INPUT_LENGTH) break;
        }

        return encodedText;
    }

    private String decodeText(Float index) {
        String word = "";

        for (String key:
                vocab.keySet()) {
            if(vocab.get(key).equals(index)) {
                word = key;
            }
        }

        if(word.equals("")) {
            word = "<pad>";
        }

        return word;
    }

    private float findHighestValue(float[] outputResult) {
        int index = 0;

        for(int i = 0; i < outputResult.length; i++) {
            index = outputResult[i] > outputResult[index] ? i : index;
        }

        return (float) index;
    }

}
