package de.johannesbock.summl.mltasks.summarisation;

import androidx.annotation.Nullable;

/**
 * This class should implement how a summary looks like. It is an immutable object, summaries should
 * stay like they were.
 * TODO: let the database hold summary elements
 */

public class TextSummarisationResult {

    private final int id;
    private final String title;
    private final String summary;
    private final float confidence;

    // some optional values
    @Nullable
    private String text;

    /**
     * This function creates an object of TextSummarisationResult. Most of the parameters are immutable.
     * @param id            An identifiable int value to make the Summary unique and recognisable.
     * @param title         The title of the object, a short version of the summary.
     * @param summary       This is the summary created by the machine learning algorithm.
     * @param confidence    A value that gives an indication of how sure the algorithm about the
     *                      quality of its result.
     * @param text          One can store the base text of the summary in this parameter.
     */
    public TextSummarisationResult(int id, String title, String summary, float confidence, String text) {
        this.id = id;
        this.title = title;
        this.summary = summary;
        this.confidence = confidence;
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public float getConfidence() {
        return confidence;
    }

    @Nullable
    public String getText() {
        return text;
    }

    public void setText(@Nullable String text) {
        this.text = text;
    }
}
