package de.johannesbock.summl.views.fragments;

import static android.content.Context.JOB_SCHEDULER_SERVICE;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.PersistableBundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.List;

import de.johannesbock.summl.R;
import de.johannesbock.summl.data.TextModel;
import de.johannesbock.summl.mltasks.MLResult;
import de.johannesbock.summl.models.SharedDataViewModel;
import de.johannesbock.summl.services.TextJobService;

/**
 * A {@link Fragment} that extends the {@link MLResultFragmentInterface} to implement all
 * methods to display the result of a summarisation. Starts the service to process the input
 * and loads the content from the database. Always loads the last text from the database in the
 * moment.
 */
public class SummaryFragment extends Fragment implements MLResultFragmentInterface {

    public final static String TAG = SummaryFragment.class.getName();

    SharedDataViewModel sharedDataViewModel;

    // the elements in the view
    private TextView titleTextView;
    private TextView summaryTextView;

    private ScrollView resultScrollView;

    private EditText inputTextEdit;

    public SummaryFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView =
                inflater.inflate(R.layout.fragment_summary, container, false);

        sharedDataViewModel = new ViewModelProvider(this).get(SharedDataViewModel.class);

        // Button handling
        Button predictButton = rootView.findViewById(R.id.summariseButton);
        predictButton.setOnClickListener(
                (View v) -> {
                    processInput(inputTextEdit.getText().toString());
                }
        );

        ImageButton imageButtonIsGood = rootView.findViewById(R.id.buttonSummaryIsGood);
        imageButtonIsGood.setOnClickListener(
                (View v) -> {
                    onLike();
                }
        );

        ImageButton imageButtonIsBad = rootView.findViewById(R.id.buttonSummaryIsBad);
        imageButtonIsBad.setOnClickListener(
                (View v) -> {
                    onDislike();
                }
        );

        // Get all the elements in the view
        titleTextView = rootView.findViewById(R.id.summaryTitleTextView);
        summaryTextView = rootView.findViewById(R.id.summaryTextView);
        inputTextEdit = rootView.findViewById(R.id.inputSummaryText);
        resultScrollView = rootView.findViewById(R.id.summaryResultsScrollView);

        showResults("", null);

        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void showResults(String inputData, List<MLResult> results) {

        sharedDataViewModel.getTextModelListLiveData()
                .observe(getViewLifecycleOwner(), new Observer<List<TextModel>>() {
                    @Override
                    public void onChanged(List<TextModel> textModels) {
                        // get last element from database
                        int lastTextInDatabase =
                                textModels.size()-1;

                        // get text from database
                        String inputData =
                                textModels.get(lastTextInDatabase).getInputData();
                        String summary =
                                textModels.get(lastTextInDatabase).getSummary();
                        String title =
                                textModels.get(lastTextInDatabase).getTitle();

                        if(summary != null) {
                            summaryTextView.setText(summary);
                        } else {
                            summaryTextView.setText("No summary available");
                        }
                        if(title != null) {
                            titleTextView.setText(title);
                        } else {
                            titleTextView.setText("Summary");
                        }
                    }
                });

    }

    @Override
    public void onLike() {

    }

    @Override
    public void onDislike() {

    }

    @Override
    public void processInput(String inputData) {
        PersistableBundle bundle = new PersistableBundle();
        bundle.putString("inputData", inputData);
        bundle.putString("textService", TextJobService.TEXT_SERVICE_SUMMARISATION);

        inputTextEdit.getText().clear();

        ComponentName componentName = new ComponentName(
                getActivity().getApplicationContext(),
                TextJobService.class);
        JobInfo info = new JobInfo.Builder(123, componentName)
                .setRequiresCharging(false)
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                .setExtras(bundle)
                .build();

        JobScheduler scheduler = (JobScheduler) getActivity().getSystemService(JOB_SCHEDULER_SERVICE);
        int resultCode = scheduler.schedule(info);
        if(resultCode == JobScheduler.RESULT_SUCCESS) {
            Log.d(TAG, "onClick: Job scheduled");
        } else {
            Log.e(TAG, "onClick: Job scheduling failed");
        }

        showResults(inputData, null);

    }
}